
#include "glut.h"
#include "MundoCliente.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
//el unico objeto global
CMundo mundo;

//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	

int main(int argc,char* argv[])
{
	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Mundo");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();
	mundo.OnDraw();
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();	

	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}


void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	if(key=='o' || key=='l' || key=='p'){
		mundo.t_sincelast=0.0f;
	}


	if(key!='q' && key !='w' && key !='s')mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
	Vector2D res;
	res.x=x+v.x;
	res.y=y+v.y;
	return res;
}
float Vector2D::operator *(const Vector2D &v)	
{
	return x*v.x+y*v.y;
}
Vector2D Vector2D::operator *(float f)
{
	Vector2D res;
	res.x=x*f;
	res.y=y*f;
	return res;
}
Vector2D Vector2D::Unitario()
{
	Vector2D retorno(x,y);
	float mod=modulo();
	if(mod>0.00001)
	{
		retorno.x/=mod;
		retorno.y/=mod;
	}
	return retorno;
}

std::ifstream& operator>>(std::ifstream& f,Vector2D& v)
{
	f>>v.x;
	f>>v.y;
	return f;
}

Vector2D Vector2D::Normal()
{
	Vector2D v;
	v.x=y;
	v.y=-x;
	return v.Unitario();
}
